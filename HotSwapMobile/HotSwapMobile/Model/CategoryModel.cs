﻿namespace HotSwapMobile.Model
{
    public class CategoryModel
    {
        public CategoryModel() { }

        public CategoryModel(int id, string name, bool isDisabled)
        {
            Id = id;
            Name = name;
            IsDisabled = isDisabled;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDisabled { get; set; }

    }
}
