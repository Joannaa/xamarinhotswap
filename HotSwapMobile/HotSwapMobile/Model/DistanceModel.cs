﻿namespace HotSwapMobile.Model
{
    public class DistanceModel
    {
        public DistanceModel() { }

        public DistanceModel(int id, int allowedDistance, bool isDisabled)
        {
            Id = id;
            AllowedDistance = allowedDistance;
            IsDisabled = isDisabled;
        }

        public int Id { get; set; }
        public int AllowedDistance { get; set; }
        public bool IsDisabled { get; set; }
        public string Name => $"{AllowedDistance} km";
    }
}