﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;
using HotSwapMobile.Interfaces;
using HotSwapMobile.Services;

namespace HotSwapMobile.ViewModel
{
    public class SplashViewModel : ViewModelBase
    {

        private readonly INaviService _navigationService;
        private readonly IRestService _restService;
        public SplashViewModel(IRestService restService, INaviService navigationService)
       {
            _restService = restService;
           _navigationService = navigationService;
       }
}
}
