﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using HotSwapMobile.Interfaces;
using HotSwapMobile.Model;
using HotSwapMobile.Services;

namespace HotSwapMobile.ViewModel
{
    public class CreateItemViewModel : ViewModelBase
    {
        private readonly INaviService _navigationService;
        private readonly IRestService _restService;

        public CreateItemViewModel(IRestService restService, INaviService navigationService)
        {
            _restService = restService;
            _navigationService = navigationService;
            CreateCategoriesAndDistances();
        }


        private string _itemName;
        public string ItemName
        {
            get { return _itemName; }
            set
            {
                if (_itemName == value) return;
                _itemName = value;
                RaisePropertyChanged(nameof(ItemName));
            }
        }

        private string _itemDescription;
        public string ItemDescription
        {
            get { return _itemDescription; }
            set
            {
                if (_itemDescription == value) return;
                _itemDescription = value;
                RaisePropertyChanged(nameof(ItemDescription));

            }
        }


        private ObservableCollection<CategoryModel> _categories;

        public ObservableCollection<CategoryModel> Categories
        {
            get { return _categories; }
            set
            {
                if (_categories == value) return;
                _categories = value;
                RaisePropertyChanged(nameof(Categories));
            }
        }

        private ObservableCollection<DistanceModel> _distances;

        public ObservableCollection<DistanceModel> Distances
        {
            get { return _distances; }
            set
            {
                if(_distances == value) return;
                _distances = value;
                RaisePropertyChanged(nameof(Distances));
            }
        }


        public void CreateCategoriesAndDistances()
        {
            Categories = new ObservableCollection<CategoryModel>
            {
                new CategoryModel(1, "Elektronika", true),
                new CategoryModel(2, "Gry", true),
            };

            Distances = new ObservableCollection<DistanceModel>
            {
                new DistanceModel(1,5,true),
                new DistanceModel(2,10,true),
                new DistanceModel(3,15,true),
            };
    }

        private CategoryModel _selectedCategory;

        public CategoryModel SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                if(_selectedCategory == value) return;
                _selectedCategory = value;
                RaisePropertyChanged(nameof(SelectedCategory));
            }
        }

        private DistanceModel _selectedDistance;

        public DistanceModel SelectedDistance
        {
            get { return _selectedDistance; }
            set
            {
                if(_selectedDistance == value) return;
                _selectedDistance = value;
                RaisePropertyChanged(nameof(SelectedDistance));
            }
        }


        public ICommand GoToSplashView => new RelayCommand(() =>
       {
           _navigationService.NavigateToAsync(AppPages.Splash);
       });

    }
}
