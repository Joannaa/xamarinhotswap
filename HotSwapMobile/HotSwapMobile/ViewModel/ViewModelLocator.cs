using GalaSoft.MvvmLight.Ioc;
using HotSwapMobile.Interfaces;
using HotSwapMobile.Model;
using HotSwapMobile.Services;
using HotSwapMobile.View;
using Microsoft.Practices.ServiceLocation;

namespace HotSwapMobile.ViewModel
{
    public class ViewModelLocator
    {
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            NavigationService = new NavigationService();
            NavigationService.Configure(AppPages.Splash,typeof(SplashView));
            SimpleIoc.Default.Register<INaviService>(() => NavigationService);
            SimpleIoc.Default.Register<IRestService, RestService>();
            SimpleIoc.Default.Register<CreateItemViewModel>();
            SimpleIoc.Default.Register<SplashViewModel>();
       
        }

        public NavigationService NavigationService;
        public CreateItemViewModel CreateItemViewModel => ServiceLocator.Current.GetInstance<CreateItemViewModel>();
        public SplashViewModel SplashViewModel => ServiceLocator.Current.GetInstance<SplashViewModel>();

    }
}