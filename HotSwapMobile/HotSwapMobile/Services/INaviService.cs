﻿using System;
using System.Threading.Tasks;
using HotSwapMobile.Model;
using Xamarin.Forms;

namespace HotSwapMobile.Services
{
    public interface INaviService
    {
        Task<Page> GoBackAsync();
        Task<Page> GoBackModalAsync();
        Task GoBackToRootAsync();
        Task NavigateToAsync(AppPages pageKey);
        Task NavigateToModalAsync(Page page);
        void Initialize(Page page);
        Task NavigateToSplashView();
        Task NavigateToCreateItemView();
        void Configure(AppPages pageKey, Type pageType);
    }
}
