﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotSwapMobile.Model;
using HotSwapMobile.View;
using Xamarin.Forms;

namespace HotSwapMobile.Services
{
    public class NavigationService : INaviService
    {
        private readonly Dictionary<AppPages, Type> _pagesByKey = new Dictionary<AppPages, Type>();
        public INavigation Navi { get; internal set; }

        public Task<Page> GoBackAsync()
        {
            return Navi.PopAsync();
        }

        public Task<Page> GoBackModalAsync()
        {
            return Navi.PopModalAsync();
        }

        public Task GoBackToRootAsync()
        {
            return Navi.PopToRootAsync();
        }

        public Task NavigateToAsync(AppPages pageKey)
        {
            if (!_pagesByKey.ContainsKey(pageKey)) return null;
            var type = _pagesByKey[pageKey];
            var page = Activator.CreateInstance(type) as ContentPage;
            return Navi.PushAsync(page);
        }

        public Task NavigateToModalAsync(Page page)
        {
            return Navi.PushModalAsync(page);
        }

        public void Initialize(Page page)
        {
            Navi = page.Navigation;
        }

        public Task NavigateToSplashView()
        {
            var page = Activator.CreateInstance(typeof(SplashView)) as ContentPage;
            return Navi.PushAsync(page);
        }

        public Task NavigateToCreateItemView()
        {
            var page = Activator.CreateInstance(typeof(CreateItemView)) as ContentPage;
            return Navi.PushAsync(page);
        }
        public void Configure(AppPages pageKey, Type pageType)
        {
            if (_pagesByKey.ContainsKey(pageKey))
            {
                _pagesByKey[pageKey] = pageType;
            }
            else
            {
                _pagesByKey.Add(pageKey, pageType);
            }
        }
    }
}
