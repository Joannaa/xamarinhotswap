﻿using Xamarin.Forms;

namespace HotSwapMobile.View
{
    public partial class CreateItemView : ContentPage
    {
        public CreateItemView()
        {
            InitializeComponent();
            BindingContext = App.Locator.CreateItemViewModel;
        }
    }
}
