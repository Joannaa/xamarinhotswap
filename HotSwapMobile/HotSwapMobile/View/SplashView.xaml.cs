﻿using Xamarin.Forms;

namespace HotSwapMobile.View
{
    public partial class SplashView : ContentPage
    {
        public SplashView()
        {
            InitializeComponent();
            BindingContext = App.Locator.SplashViewModel;
        }
    }
}
